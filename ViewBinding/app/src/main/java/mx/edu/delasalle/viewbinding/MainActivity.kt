package mx.edu.delasalle.viewbinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import mx.edu.delasalle.viewbinding.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    //Para utilizar el viewBinding
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        /*
        //Forma normal
        val btn = findViewById<Button>(R.id.main_activity_bn_1)

        btn.setOnClickListener{
            Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show()
        }*/


        //Forma con viewBinding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.mainActivityBn1.setOnClickListener{
            Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show()
        }

    }
}