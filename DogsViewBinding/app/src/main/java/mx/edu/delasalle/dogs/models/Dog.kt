package mx.edu.delasalle.dogs.models

import java.io.Serializable

class Dog (val name:String, val imageUrl:String): Serializable
